// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use itertools::Itertools;
use std::{io::Read, str::FromStr};

fn count_calories(s: &str) -> Vec<usize> {
    s.lines()
        .group_by(|s| s.is_empty())
        .into_iter()
        .filter_map(|g| g.1.filter_map(|s| usize::from_str(s).ok()).sum1())
        .collect()
}

fn main() -> anyhow::Result<()> {
    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;
    let mut calories = count_calories(&s);
    calories.sort();
    let max_three = calories.into_iter().rev().take(3).sum::<usize>();
    println!("{}", max_three);
    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn count_calories() {
        let example = include_str!("example.txt");
        assert_eq!(
            super::count_calories(example),
            vec![6000, 4000, 11000, 24000, 10000]
        );
    }
}
