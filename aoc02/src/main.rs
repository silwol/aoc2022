// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{bail, Context, Result};
use log::warn;
use std::io::Read;

#[derive(Debug, Clone, Copy)]
enum HandShape {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug, Clone, Copy)]
enum Outcome {
    Win,
    Draw,
    Lose,
}

fn play_round(opponent_shape: HandShape, my_shape: HandShape) -> Outcome {
    match (opponent_shape, my_shape) {
        (HandShape::Rock, HandShape::Paper)
        | (HandShape::Paper, HandShape::Scissors)
        | (HandShape::Scissors, HandShape::Rock) => Outcome::Win,
        (HandShape::Rock, HandShape::Rock)
        | (HandShape::Paper, HandShape::Paper)
        | (HandShape::Scissors, HandShape::Scissors) => Outcome::Draw,
        (HandShape::Paper, HandShape::Rock)
        | (HandShape::Scissors, HandShape::Paper)
        | (HandShape::Rock, HandShape::Scissors) => Outcome::Lose,
    }
}

trait Score {
    fn score(self) -> usize;
}

impl Score for HandShape {
    fn score(self) -> usize {
        match self {
            HandShape::Rock => 1,
            HandShape::Paper => 2,
            HandShape::Scissors => 3,
        }
    }
}

impl Score for Outcome {
    fn score(self) -> usize {
        match self {
            Outcome::Win => 6,
            Outcome::Draw => 3,
            Outcome::Lose => 0,
        }
    }
}

impl HandShape {
    fn get_mine_for_outcome(self, outcome: Outcome) -> HandShape {
        match (self, outcome) {
            (HandShape::Rock, Outcome::Win)
            | (HandShape::Paper, Outcome::Draw)
            | (HandShape::Scissors, Outcome::Lose) => HandShape::Paper,

            (HandShape::Paper, Outcome::Win)
            | (HandShape::Scissors, Outcome::Draw)
            | (HandShape::Rock, Outcome::Lose) => HandShape::Scissors,

            (HandShape::Scissors, Outcome::Win)
            | (HandShape::Rock, Outcome::Draw)
            | (HandShape::Paper, Outcome::Lose) => HandShape::Rock,
        }
    }
}

fn parse_handshape(s: &str) -> Result<HandShape> {
    match s {
        "A" => Ok(HandShape::Rock),
        "B" => Ok(HandShape::Paper),
        "C" => Ok(HandShape::Scissors),
        _ => bail!("Unknown opponent hand shape code: {:?}", s),
    }
}

fn parse_outcome(s: &str) -> Result<Outcome> {
    match s {
        "X" => Ok(Outcome::Lose),
        "Y" => Ok(Outcome::Draw),
        "Z" => Ok(Outcome::Win),
        _ => bail!("Unknown opponent hand shape code: {:?}", s),
    }
}

fn parse_line(line: &str) -> Result<(HandShape, Outcome)> {
    let (opponent, outcome) = line
        .split_once(' ')
        .context("Line must have at least two entries")?;

    Ok((parse_handshape(opponent)?, parse_outcome(outcome)?))
}

fn play_game(input: &str) -> usize {
    input
        .lines()
        .map(parse_line)
        .filter_map(|v| match v {
            Ok(v) => Some(v),
            Err(e) => {
                warn!("Error reading line: {:?}", e);
                None
            }
        })
        .map(|(opponent_handshape, outcome)| {
            let mine = opponent_handshape.get_mine_for_outcome(outcome);
            mine.score() + play_round(opponent_handshape, mine).score()
        })
        .sum::<usize>()
}

fn main() -> anyhow::Result<()> {
    env_logger::init();

    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;
    println!("{}", play_game(&s));
    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn play() {
        let example = include_str!("example.txt");
        assert_eq!(super::play_game(example), 12);
    }
}
