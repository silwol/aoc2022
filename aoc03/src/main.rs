// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{bail, Context, Result};
use std::{
    collections::{HashMap, HashSet},
    io::Read,
};

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
struct Item {
    weight: usize,
}

impl Item {
    fn weight(&self) -> usize {
        self.weight
    }
}

impl TryFrom<char> for Item {
    type Error = anyhow::Error;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c as u8 {
            b @ b'a'..=b'z' => Ok(Self {
                weight: (1 + b - b'a') as usize,
            }),
            b @ b'A'..=b'Z' => Ok(Self {
                weight: (27 + b - b'A') as usize,
            }),
            b => bail!("Invalid item {:?}", b),
        }
    }
}

struct Rucksack {
    compartments: [Vec<Item>; 2],
}

impl Rucksack {
    fn compartment_contents_to_hash_map(
        compartment: &[Item],
    ) -> HashMap<Item, usize> {
        let mut map = HashMap::new();
        for item in compartment {
            (*map.entry(item.clone()).or_insert(0)) += 1;
        }
        map
    }

    fn get_common_item_priorities(&self) -> usize {
        let a =
            Self::compartment_contents_to_hash_map(&self.compartments[0]);
        let b =
            Self::compartment_contents_to_hash_map(&self.compartments[1]);

        let a: HashSet<Item> = HashSet::from_iter(a.keys().cloned());
        let b: HashSet<Item> = HashSet::from_iter(b.keys().cloned());

        let intersection = a.intersection(&b);

        intersection.into_iter().map(|i| i.weight()).sum()
    }

    fn contents_to_hash_map(&self) -> HashMap<Item, usize> {
        let mut map = HashMap::new();
        for item in self.compartments.iter().flatten() {
            (*map.entry(item.clone()).or_insert(0)) += 1;
        }
        map
    }

    fn contents_to_hash_set(&self) -> HashSet<Item> {
        HashSet::from_iter(self.contents_to_hash_map().keys().cloned())
    }
}

impl TryFrom<&str> for Rucksack {
    type Error = anyhow::Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let (first, second) = s.split_at(s.len() / 2);

        let first = first
            .chars()
            .map(Item::try_from)
            .collect::<Result<Vec<_>>>()?;
        let second = second
            .chars()
            .map(Item::try_from)
            .collect::<Result<Vec<_>>>()?;

        Ok(Self {
            compartments: [first, second],
        })
    }
}

fn load_rucksacks(s: &str) -> Result<Vec<Rucksack>> {
    s.lines()
        .map(Rucksack::try_from)
        .collect::<Result<Vec<_>>>()
}

fn find_badge(rucksacks: &[Rucksack]) -> Result<Item> {
    let mut items: Option<HashSet<Item>> = None;
    for rucksack in rucksacks {
        let hash_set = rucksack.contents_to_hash_set();
        items = Some(if let Some(items) = items {
            items.intersection(&hash_set).cloned().collect()
        } else {
            hash_set
        });
    }
    items
        .context("No rucksacks found")?
        .into_iter()
        .next()
        .context("No common items found")
}

fn main() -> Result<()> {
    env_logger::init();

    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;

    let rucksacks = load_rucksacks(&s)?;

    let mut sum = 0usize;
    for group in rucksacks.chunks(3) {
        let badge = find_badge(group)?;
        sum += badge.weight();
    }

    println!("{}", sum);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn priorities() -> Result<()> {
        assert_eq!(Item::try_from('a')?.weight(), 1);
        assert_eq!(Item::try_from('z')?.weight(), 26);

        assert_eq!(Item::try_from('A')?.weight(), 27);
        assert_eq!(Item::try_from('Z')?.weight(), 52);

        assert_eq!(Item::try_from('p')?.weight(), 16);
        assert_eq!(Item::try_from('L')?.weight(), 38);
        assert_eq!(Item::try_from('v')?.weight(), 22);
        assert_eq!(Item::try_from('t')?.weight(), 20);
        assert_eq!(Item::try_from('s')?.weight(), 19);

        Ok(())
    }

    #[test]
    fn example() -> Result<()> {
        let example = include_str!("example.txt");

        let rucksacks = load_rucksacks(example)?;

        let overlapping_sum: usize = rucksacks
            .iter()
            .map(|r| r.get_common_item_priorities())
            .sum();

        assert_eq!(overlapping_sum, 157);

        Ok(())
    }
}
