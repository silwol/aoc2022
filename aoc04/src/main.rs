// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{Context, Result};
use regex::Regex;
use std::io::Read;

macro_rules! regex {
    ($re:literal $(,)?) => {{
        static RE: once_cell::sync::OnceCell<regex::Regex> =
            once_cell::sync::OnceCell::new();
        RE.get_or_init(|| regex::Regex::new($re).unwrap())
    }};
}

fn parse_line(line: &str) -> Result<((usize, usize), (usize, usize))> {
    let re: &Regex = regex!(r##"^(\d+)-(\d+),(\d+)-(\d+)$"##);

    let c = re.captures(line).context("Line doesn't match pattern")?;

    Ok((
        (c[1].parse()?, c[2].parse()?),
        (c[3].parse()?, c[4].parse()?),
    ))
}

trait Region {
    fn fully_contains(&self, other: &Self) -> bool;
    fn overlaps(&self, other: &Self) -> bool;
}

impl Region for (usize, usize) {
    fn fully_contains(&self, other: &Self) -> bool {
        let r = self.0..=self.1;

        r.contains(&other.0) && r.contains(&other.1)
    }

    fn overlaps(&self, other: &Self) -> bool {
        let r = self.0..=self.1;

        one_fully_contains_the_other(*self, *other)
            || r.contains(&other.0)
            || r.contains(&other.1)
    }
}

fn one_fully_contains_the_other<R: Region>(a: R, b: R) -> bool {
    a.fully_contains(&b) || b.fully_contains(&a)
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;

    let overlaps = s
        .lines()
        .filter(|l| {
            let (r1, r2) = parse_line(l).ok().unwrap();
            r1.overlaps(&r2)
        })
        .count();
    println!("{}", overlaps);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_lines() -> Result<()> {
        assert_eq!(parse_line("1-2,3-4")?, ((1, 2), (3, 4)));
        assert_eq!(parse_line("11-23,35-40")?, ((11, 23), (35, 40)));
        assert_eq!(parse_line("1-0,0-3")?, ((1, 0), (0, 3)));
        Ok(())
    }

    #[test]
    fn fully_contains() {
        assert!((1, 3).fully_contains(&(2, 3)));
        assert!(!(3, 3).fully_contains(&(2, 3)));
        assert!((10, 20).fully_contains(&(12, 16)));
    }

    #[test]
    fn overlaps() {
        assert!(!(2, 4).overlaps(&(6, 8)));
        assert!(!(3, 3).fully_contains(&(2, 3)));
        assert!((10, 20).fully_contains(&(12, 16)));
    }
}
