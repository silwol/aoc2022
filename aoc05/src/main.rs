// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{Context, Result};
use regex::Regex;
use std::io::Read;

macro_rules! regex {
    ($re:literal $(,)?) => {{
        static RE: once_cell::sync::OnceCell<regex::Regex> =
            once_cell::sync::OnceCell::new();
        RE.get_or_init(|| regex::Regex::new($re).unwrap())
    }};
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Crate {
    identity: char,
}

impl Crate {
    fn new(identity: char) -> Self {
        Self { identity }
    }
}

struct Stack {
    crates: Vec<Crate>,
}

impl Stack {
    fn new() -> Self {
        Self { crates: Vec::new() }
    }

    fn put_on_top(&mut self, mut cr: Vec<Crate>) {
        self.crates.append(&mut cr);
    }

    fn top_crate(&self) -> Option<Crate> {
        self.crates.iter().last().copied()
    }

    fn take_from_top(&mut self, count: usize) -> Vec<Crate> {
        let height = self.crates.len();
        self.crates.split_off(height - count)
    }
}

struct Storage {
    stacks: Vec<Stack>,
}

impl Storage {
    fn new(size: usize) -> Self {
        Self {
            stacks: std::iter::repeat_with(Stack::new)
                .take(size)
                .collect(),
        }
    }

    fn load_stack_row(&mut self, row: &[Option<Crate>]) {
        for (stack, cr) in self.stacks.iter_mut().zip(row) {
            if let Some(cr) = cr {
                stack.put_on_top(vec![*cr]);
            }
        }
    }

    fn top_crates(&self) -> Vec<Option<Crate>> {
        self.stacks.iter().map(Stack::top_crate).collect()
    }

    fn move_from_to(
        &mut self,
        count: usize,
        from: usize,
        to: usize,
    ) -> Result<()> {
        let cr = self
            .stacks
            .get_mut(from)
            .context("invalid from stack index")?
            .take_from_top(count);
        self.stacks
            .get_mut(to)
            .context("invalid to stack index")?
            .put_on_top(cr);
        Ok(())
    }

    fn process_command(&mut self, command: &Command) -> Result<()> {
        self.move_from_to(
            command.count,
            command.from - 1,
            command.to - 1,
        )?;
        Ok(())
    }
}

fn parse_stack_row(l: &str) -> Result<Vec<Option<Crate>>> {
    let re: &Regex = regex!(r##"^(\[(?P<id>[A-Z])\])"##);

    let mut i = 0;

    let mut crates = Vec::new();

    while i < l.len() {
        let s = &l[i..i + 3];
        if s.trim().is_empty() {
            crates.push(None);
        } else {
            let captures = re.captures(s).context("invalid crate line")?;
            crates.push(Some(Crate::new(
                captures["id"].chars().next().unwrap(),
            )));
        }
        i += 4;
    }
    Ok(crates)
}

fn parse_stack_base(l: &str) -> usize {
    l.split_whitespace().count()
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Command {
    from: usize,
    to: usize,
    count: usize,
}

fn parse_command(l: &str) -> Result<Command> {
    let re: &Regex = regex!(
        r##"^move (?P<count>\d+) from (?P<from>\d+) to (?P<to>\d+)$"##
    );
    let captures = re.captures(l).context("invalid command")?;
    let count = captures["count"].parse()?;
    let from = captures["from"].parse()?;
    let to = captures["to"].parse()?;

    Ok(Command { from, to, count })
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;

    let count = s.lines().count();
    let storage_lines = s.lines().position(str::is_empty).unwrap();
    let command_lines = count - storage_lines;

    let mut stack_description_iter = s.lines().rev().skip(command_lines);
    let mut storage = Storage::new(parse_stack_base(
        stack_description_iter
            .next()
            .context("no stack base found")?,
    ));

    for line in stack_description_iter {
        let stack_row =
            parse_stack_row(line).context("invalid stack row")?;
        storage.load_stack_row(&stack_row);
    }

    println!(
        "{}",
        storage
            .top_crates()
            .into_iter()
            .map(|c| c.map(|c| c.identity).unwrap_or(' '))
            .collect::<String>()
    );

    for line in s.lines().skip(storage_lines + 1) {
        let command =
            parse_command(line).context("invalid command line")?;
        storage.process_command(&command)?;
    }

    println!(
        "{}",
        storage
            .top_crates()
            .into_iter()
            .map(|c| c.map(|c| c.identity).unwrap_or(' '))
            .collect::<String>()
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_stack_row() {
        assert_eq!(
            super::parse_stack_row("    [D]    ").unwrap(),
            vec![None, Some(Crate::new('D')), None]
        );
        assert_eq!(
            super::parse_stack_row("[N] [C]    ").unwrap(),
            vec![Some(Crate::new('N')), Some(Crate::new('C')), None]
        );
        assert_eq!(
            super::parse_stack_row("[Z] [M] [P]").unwrap(),
            vec![
                Some(Crate::new('Z')),
                Some(Crate::new('M')),
                Some(Crate::new('P'))
            ]
        );
    }

    #[test]
    fn parse_command() {
        assert_eq!(
            super::parse_command("move 1 from 2 to 1").unwrap(),
            Command {
                from: 2,
                to: 1,
                count: 1
            }
        );
        assert_eq!(
            super::parse_command("move 3 from 1 to 3").unwrap(),
            Command {
                from: 1,
                to: 3,
                count: 3
            }
        );
        assert_eq!(
            super::parse_command("move 2 from 2 to 1").unwrap(),
            Command {
                from: 2,
                to: 1,
                count: 2
            }
        );
        assert_eq!(
            super::parse_command("move 1 from 1 to 2").unwrap(),
            Command {
                from: 1,
                to: 2,
                count: 1
            }
        );
    }
}
