// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::Result;
use std::{collections::BTreeSet, io::Read};

fn find_start_of_packet(s: &str, length: usize) -> Option<usize> {
    s.as_bytes()
        .windows(length)
        .position(|w| BTreeSet::from_iter(w.iter()).len() == length)
        .map(|i| i + length)
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;

    if let Some(i) = find_start_of_packet(&s, 14) {
        println!("{}", i);
    } else {
        println!("No marker found");
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_start() -> Result<()> {
        assert_eq!(find_start_of_packet("", 4), None);
        assert_eq!(
            find_start_of_packet("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 4),
            Some(7)
        );
        assert_eq!(
            find_start_of_packet("bvwbjplbgvbhsrlpgdmjqwftvncz", 4),
            Some(5)
        );
        assert_eq!(
            find_start_of_packet("nppdvjthqldpwncqszvftbrmjlhg", 4),
            Some(6)
        );
        assert_eq!(
            find_start_of_packet("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 4),
            Some(10)
        );
        assert_eq!(
            find_start_of_packet("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 4),
            Some(11)
        );
        assert_eq!(
            find_start_of_packet("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14),
            Some(19)
        );
        assert_eq!(
            find_start_of_packet("bvwbjplbgvbhsrlpgdmjqwftvncz", 14),
            Some(23)
        );
        assert_eq!(
            find_start_of_packet("nppdvjthqldpwncqszvftbrmjlhg", 14),
            Some(23)
        );
        assert_eq!(
            find_start_of_packet("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 14),
            Some(29)
        );
        assert_eq!(
            find_start_of_packet("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 14),
            Some(26)
        );
        Ok(())
    }
}
