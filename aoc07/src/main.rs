// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{bail, Context, Result};
use std::{collections::BTreeMap, io::Read, path::PathBuf, str::FromStr};

peg::parser! {
    grammar log_parser() for str {
        rule path() -> PathBuf
          = s:$(['a'..='z' | 'A'..='Z' | '0'..='9' | '.' | '_' | '/']+)
        {
            PathBuf::from(s)
        }

        rule command_change_directory() -> Command
          = "cd " p:path()
        {
            Command::ChangeDirectory(p)
        }

        rule command_list() -> Command
          = "ls"
        {
            Command::List
        }

        rule command() -> Command
          = command_change_directory() / command_list()

        rule command_line() -> Command
          = "$ " c:command()
        {
            c
        }

        rule list_output_directory() -> ListOutput
          = "dir " p:path()
        {
            ListOutput::Directory(p)
        }

        rule file_size() -> usize
          = n:$(['0'..='9']+)
        {?
            usize::from_str(n).or(Err("number"))
        }

        rule list_output_file() -> ListOutput
          = size:file_size() " " name:path()
        {
            ListOutput::File{name,size}
        }

        rule list_output() -> ListOutput
          = list_output_directory() / list_output_file()

        rule log_line_command() -> LogLine
          = c:command_line()
        {
            LogLine::Command(c)
        }

        rule log_line_list_output() -> LogLine
          = l:list_output()
        {
            LogLine::ListOutput(l)
        }

        rule log_line() -> LogLine
          = log_line_command() / log_line_list_output()

        pub(super) rule log() -> Vec<LogLine>
          = l:log_line() **<1,> "\n"
          "\n"?
        {
            l
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum Command {
    ChangeDirectory(PathBuf),
    List,
}

#[derive(Debug, PartialEq, Eq)]
enum ListOutput {
    Directory(PathBuf),
    File { name: PathBuf, size: usize },
}

#[derive(Debug, PartialEq, Eq)]
enum LogLine {
    Command(Command),
    ListOutput(ListOutput),
}

fn parse(s: &str) -> Result<Vec<LogLine>> {
    Ok(log_parser::log(s)?)
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum DirectoryEntry {
    Directory(BTreeMap<PathBuf, DirectoryEntry>),
    File { size: usize },
}

impl Default for DirectoryEntry {
    fn default() -> Self {
        DirectoryEntry::Directory(BTreeMap::default())
    }
}

impl DirectoryEntry {
    fn is_directory(&self) -> bool {
        matches!(self, DirectoryEntry::Directory(_))
    }

    fn insert_at(&mut self, path: &[PathBuf], entry: Self) -> Result<()> {
        let mut d: &mut Self = self;
        for (i, p) in path.iter().enumerate() {
            if let DirectoryEntry::Directory(children) = d {
                let is_last = i == path.len() - 1;
                let new_entry = if is_last {
                    entry.clone()
                } else {
                    Default::default()
                };
                d = children.entry(p.to_path_buf()).or_insert(new_entry);
            } else {
                bail!("Invalid entry");
            }
        }
        Ok(())
    }

    fn recursive_size(&self) -> usize {
        match self {
            DirectoryEntry::Directory(children) => {
                children.values().map(Self::recursive_size).sum()
            }
            DirectoryEntry::File { size } => *size,
        }
    }

    fn walk(
        &self,
        path: &[PathBuf],
        callback: &mut dyn FnMut(&[PathBuf], &Self),
    ) {
        let mut path = Vec::from_iter(path.iter().cloned());
        callback(&path, self);
        if let DirectoryEntry::Directory(children) = self {
            for (subpath, child) in children {
                path.push(subpath.to_path_buf());
                child.walk(&path, callback);
                path.pop();
            }
        }
    }

    fn build_from_log(log: &[LogLine]) -> Result<Self> {
        let mut current_location = vec![];

        let mut tree = DirectoryEntry::Directory(BTreeMap::new());

        for line in log {
            match line {
                LogLine::Command(Command::ChangeDirectory(p)) => {
                    match p.to_str().context("invalid path str")? {
                        "/" => {
                            current_location.clear();
                        }
                        ".." => {
                            current_location
                                .pop()
                                .context("Already at toplevel")?;
                        }
                        _ => {
                            current_location.push(p.to_path_buf());
                        }
                    }
                }
                LogLine::Command(Command::List) => {
                    // Ignoring for now, no consistency checks
                }
                LogLine::ListOutput(o) => {
                    match o {
                        ListOutput::File { name, size } => {
                            current_location.push(name.to_path_buf());
                            tree.insert_at(
                                &current_location,
                                DirectoryEntry::File { size: *size },
                            )?;
                            current_location.pop();
                        }
                        ListOutput::Directory(name) => {
                            current_location.push(name.to_path_buf());
                            tree.insert_at(
                                &current_location,
                                DirectoryEntry::Directory(
                                    Default::default(),
                                ),
                            )?;
                            current_location.pop();
                        }
                    };
                }
            }
        }

        Ok(tree)
    }

    fn all_sizes_up_to(&self, max: usize) -> usize {
        let mut sizes = Vec::new();
        self.walk(&vec![], &mut |_p, e: &DirectoryEntry| {
            if e.is_directory() {
                sizes.push(e.recursive_size());
            }
        });
        sizes.into_iter().filter(|v| *v <= max).sum::<usize>()
    }

    fn smallest_above(&self, minimum: usize) -> usize {
        let mut smallest = self.recursive_size();
        self.walk(&vec![], &mut |_p, e: &DirectoryEntry| {
            if e.is_directory() {
                let size = e.recursive_size();
                if size < smallest && size >= minimum {
                    smallest = size;
                }
            }
        });
        smallest
    }
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;

    let log = parse(&s)?;

    let entry = DirectoryEntry::build_from_log(&log)?;

    println!("{}", entry.all_sizes_up_to(100000));

    let overall_space = 70000000;
    let minimum_required = 30000000;

    let used = entry.recursive_size();
    let free = overall_space - used;

    let must_be_freed = minimum_required - free;

    println!("{}", entry.smallest_above(must_be_freed));
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn example_log() -> Vec<LogLine> {
        vec![
            LogLine::Command(Command::ChangeDirectory(PathBuf::from("/"))),
            LogLine::Command(Command::List),
            LogLine::ListOutput(ListOutput::Directory(PathBuf::from("a"))),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("b.txt"),
                size: 14848514,
            }),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("c.dat"),
                size: 8504156,
            }),
            LogLine::ListOutput(ListOutput::Directory(PathBuf::from("d"))),
            LogLine::Command(Command::ChangeDirectory(PathBuf::from("a"))),
            LogLine::Command(Command::List),
            LogLine::ListOutput(ListOutput::Directory(PathBuf::from("e"))),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("f"),
                size: 29116,
            }),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("g"),
                size: 2557,
            }),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("h.lst"),
                size: 62596,
            }),
            LogLine::Command(Command::ChangeDirectory(PathBuf::from("e"))),
            LogLine::Command(Command::List),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("i"),
                size: 584,
            }),
            LogLine::Command(Command::ChangeDirectory(PathBuf::from(
                "..",
            ))),
            LogLine::Command(Command::ChangeDirectory(PathBuf::from(
                "..",
            ))),
            LogLine::Command(Command::ChangeDirectory(PathBuf::from("d"))),
            LogLine::Command(Command::List),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("j"),
                size: 4060174,
            }),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("d.log"),
                size: 8033020,
            }),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("d.ext"),
                size: 5626152,
            }),
            LogLine::ListOutput(ListOutput::File {
                name: PathBuf::from("k"),
                size: 7214296,
            }),
        ]
    }

    #[test]
    fn parse() {
        let example = include_str!("example.txt");
        assert_eq!(super::parse(example).unwrap(), example_log())
    }

    #[test]
    fn sizes() -> Result<()> {
        let sizes = DirectoryEntry::build_from_log(&example_log())?
            .all_sizes_up_to(1000000);
        println!("{}", sizes);
        Ok(())
    }
}
