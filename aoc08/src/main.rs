use anyhow::{ensure, Context, Result};
use std::{collections::BTreeMap, io::Read, str::FromStr};

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Tree(usize);

impl TryFrom<char> for Tree {
    type Error = anyhow::Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        ensure!(
            value.is_ascii_digit(),
            "Tree value must be an ascii digit"
        );
        Ok(Tree((value as usize) - '0' as usize))
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum ViewDirection {
    North,
    West,
    South,
    East,
}

fn all_directions() -> Vec<ViewDirection> {
    vec![
        ViewDirection::North,
        ViewDirection::West,
        ViewDirection::South,
        ViewDirection::East,
    ]
}

impl ViewDirection {
    #[must_use]
    fn apply_to_position(&self, pos: &mut (usize, usize)) -> bool {
        match self {
            ViewDirection::North => {
                if pos.1 > 0 {
                    pos.1 -= 1;
                    true
                } else {
                    false
                }
            }
            ViewDirection::West => {
                pos.0 += 1;
                true
            }
            ViewDirection::South => {
                pos.1 += 1;
                true
            }
            ViewDirection::East => {
                if pos.0 > 0 {
                    pos.0 -= 1;
                    true
                } else {
                    false
                }
            }
        }
    }
}

#[derive(Debug)]
struct ForestTreeView<'a> {
    forest: &'a Forest,
    direction: ViewDirection,
    position: (usize, usize),
    finished: bool,
}

impl<'a> Iterator for ForestTreeView<'a> {
    type Item = Tree;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            None
        } else {
            let item = self.forest.trees.get(&self.position).copied();
            self.finished =
                !self.direction.apply_to_position(&mut self.position);

            item
        }
    }
}

/// A forest containing trees on a cartesian coordinate system
/// The zero point of the coordinate system is top left,
///  y increases towards the bottom, x increases towards the right side.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
struct Forest {
    trees: BTreeMap<(usize, usize), Tree>,
}

impl Forest {
    fn view<'a>(
        &'a self,
        x: usize,
        y: usize,
        direction: ViewDirection,
    ) -> ForestTreeView<'a> {
        ForestTreeView {
            forest: self,
            direction,
            position: (x, y),
            finished: false,
        }
    }

    fn is_tree_visible(&self, x: usize, y: usize) -> bool {
        for dir in all_directions() {
            let mut view = self.view(x, y, dir);
            let mut visible = true;
            let own_height = if let Some(tree) = view.next() {
                tree
            } else {
                return false;
            };
            for tree in view {
                if tree >= own_height {
                    visible = false;
                }
            }
            if visible {
                return true;
            }
        }
        false
    }

    fn visible_tree_count(&self) -> usize {
        let mut count = 0usize;
        for (x, y) in self.trees.keys() {
            if self.is_tree_visible(*x, *y) {
                count += 1;
            }
        }
        count
    }

    fn visible_trees(
        &self,
        x: usize,
        y: usize,
        direction: ViewDirection,
    ) -> usize {
        let mut view = self.view(x, y, direction);
        let own_height = if let Some(tree) = view.next() {
            tree
        } else {
            return 0;
        };
        let mut blocked = false;
        view.take_while(|t| {
            if blocked {
                false
            } else {
                blocked |= *t >= own_height;
                true
            }
        })
        .count()
    }

    fn scenic_score(&self, x: usize, y: usize) -> usize {
        all_directions()
            .into_iter()
            .map(|d| self.visible_trees(x, y, d))
            .product()
    }

    fn max_scenic_score(&self) -> usize {
        let mut max = 0;
        for (x, y) in self.trees.keys() {
            max = max.max(self.scenic_score(*x, *y));
        }
        max
    }
}

impl FromStr for Forest {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut trees = BTreeMap::new();
        for (y, l) in s.lines().enumerate() {
            for (x, c) in l.chars().enumerate() {
                trees.insert(
                    (x, y),
                    Tree::try_from(c)
                        .context(format!("line {y} col {x}"))?,
                );
            }
        }
        Ok(Forest { trees })
    }
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin().lock();
    let mut s = String::new();
    stdin.read_to_string(&mut s)?;

    let forest = Forest::from_str(&s)?;

    println!("{}", forest.visible_tree_count());
    println!("{}", forest.max_scenic_score());
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    impl Forest {
        fn set_row(&mut self, y: usize, row: &[Tree]) {
            for (x, tree) in row.iter().enumerate() {
                self.trees.insert((x, y), *tree);
            }
        }
    }

    fn example_forest() -> Forest {
        let mut forest = Forest::default();
        forest.set_row(
            0,
            &vec![Tree(3), Tree(0), Tree(3), Tree(7), Tree(3)],
        );
        forest.set_row(
            1,
            &vec![Tree(2), Tree(5), Tree(5), Tree(1), Tree(2)],
        );
        forest.set_row(
            2,
            &vec![Tree(6), Tree(5), Tree(3), Tree(3), Tree(2)],
        );
        forest.set_row(
            3,
            &vec![Tree(3), Tree(3), Tree(5), Tree(4), Tree(9)],
        );
        forest.set_row(
            4,
            &vec![Tree(3), Tree(5), Tree(3), Tree(9), Tree(0)],
        );
        forest
    }

    #[test]
    fn parse() {
        let example = include_str!("example.txt");
        assert_eq!(Forest::from_str(example).unwrap(), example_forest())
    }

    #[test]
    fn visibility() {
        let forest = example_forest();

        assert!(forest.is_tree_visible(0, 0));
        assert!(forest.is_tree_visible(0, 1));
        assert!(forest.is_tree_visible(0, 2));
        assert!(forest.is_tree_visible(0, 3));
        assert!(forest.is_tree_visible(0, 4));

        assert!(forest.is_tree_visible(1, 0));
        assert!(forest.is_tree_visible(1, 1));
        assert!(forest.is_tree_visible(1, 2));
        assert!(!forest.is_tree_visible(1, 3));
        assert!(forest.is_tree_visible(1, 4));

        assert!(forest.is_tree_visible(2, 0));
        assert!(forest.is_tree_visible(2, 1));
        assert!(!forest.is_tree_visible(2, 2));
        assert!(forest.is_tree_visible(2, 3));
        assert!(forest.is_tree_visible(2, 4));

        assert!(forest.is_tree_visible(3, 0));
        assert!(!forest.is_tree_visible(3, 1));
        assert!(forest.is_tree_visible(3, 2));
        assert!(!forest.is_tree_visible(3, 3));
        assert!(forest.is_tree_visible(3, 4));

        assert!(forest.is_tree_visible(4, 0));
        assert!(forest.is_tree_visible(4, 1));
        assert!(forest.is_tree_visible(4, 2));
        assert!(forest.is_tree_visible(4, 3));
        assert!(forest.is_tree_visible(4, 4));

        assert_eq!(forest.visible_tree_count(), 21)
    }

    #[test]
    fn scenic_score() {
        let forest = example_forest();

        assert_eq!(forest.scenic_score(2, 1), 4);
        assert_eq!(forest.scenic_score(2, 3), 8);

        assert_eq!(forest.max_scenic_score(), 8);
    }
}
